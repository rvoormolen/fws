import re

import logging

import warnings

class LineParser(object):
    CLASSMAP = []
    log = logging.getLogger('LineParser')

    def __init__(self, fwsparser):
        self.buf = []
        self.fwsparser = fwsparser

    @classmethod
    def register(cls, *regexes):
        def register_regex(obj):
            for regex in regexes:
                cls.log.debug("Registered %s with %s", obj.__name__, regex)
                cls.CLASSMAP.append((re.compile(regex), obj))
            return obj
        return register_regex

    def __iadd__(self, data):
        data = data.strip()
        # Ignore empty lines
        if len(data) == 0:
            return self

        # Ignore comments
        if data.startswith('#'):
            return self

        self.buf.append(data)
        return self

    def endofline(self):
        if not self.hasdata():
            return False

        if self.buf[-1][-1:] == '\\':
            # Clean up
            self.buf[-1] = self.buf[-1][0:-1]
            return False
        return True
    
    def parse(self):
        # Clean up lines
        line = ' '.join(self.buf)
        for parser, cls in self.CLASSMAP:
            match = parser.match(line)
            if match:
                self.log.debug('Match on "%s" with "%r"', line, match.groupdict())
                return cls(line, match)
                break
        else:
            warnings.warn("No parser found for: %s" % line)
        return True

    def hasdata(self):
        """ Check if we have a line and if it is end of the line """
        if len(self.buf) == 0:
            # No line
            return False
        return True

class LineObject(object):
    def __init__(self, line, regex_match):
        self.line = line
        self.regex_match = regex_match

    def __repr__(self):
        return "<%s::%s>" % (self.__class__.__name__, self.line)
