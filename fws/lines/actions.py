from . import LineParser, LineObject

@LineParser.register(r"^(?P<action>pass|drop|reject)\s+(?P<direction>in(?:put)?|forward|out(?:put)?)\s+")
class ActionLine(LineObject):
    def __init__(self, line, regex_match):
        super().__init__(line, regex_match)
        self.action = regex_match.group('action')
        self.direction = regex_match.group('direction')

@LineParser.register(r"^policy\s+(?P<direction>in(?:put)?|forward|out(?:put)?)\s+(?P<arguments>.*)")
class PolicyLine(LineObject):
    def __init__(self, line, regex):
        super().__init__(line, regex)
        self.arguments = regex.group('arguments').split()
        self.direction = regex.group('direction')
