from . import LineParser, LineObject

@LineParser.register(
    r"^(?P<key>\w+)\s*=\s*\"(?P<value>.*)\"$",
    r"^(?P<key>\w+)\s*=\s*'(?P<value>.*)'$")
class MacroLine(LineObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.key = self.regex_match.group('key')
        self.value = self.regex_match.group('value')

    def __repr__(self):
        return "<%s::[%s=%s]>" % (self.__class__.__name__, self.key, self.value)

@LineParser.register(r"^(?P<key>\w+)\s*=\s*<(?P<values>.*)>$")
class MacroListLine(LineObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.key = self.regex_match.group('key')
        self.values = [ val.strip() for val in self.regex_match.group('values').split(',') ]

    def __repr__(self):
        return "<%s::[%s=%r]>" % (self.__class__.__name__, self.key, self.values)
