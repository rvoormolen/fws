from time import time as now
from .lines import LineParser
from .lines.actions import ActionLine, PolicyLine
from .lines.macros import MacroLine, MacroListLine

# Remove for production
import pprint

class FWSParser():
    def __init__(self, fdscript):
        """ Creates a Firewall Script parser

        Arguments:
          - fdscript - Open filedescriptor to firewall script
        """

        self.fd = fdscript
        self.lines = []
        self._parse_time = None

    def parse(self):
        start_time = now()

        parser = LineParser(self)
        for line in self.fd:
            parser += line
            if parser.endofline():
                self.lines.append(parser.parse())
                parser = LineParser(self)
        
        self._parse_time = now() - start_time
        print("Parsed in %0.5f seconds" % self._parse_time)
        return self

    @property
    def macros(self):
        return [ line for line in self.lines if isinstance(line, MacroLine) ]

    @property
    def macro_lists(self):
        return [ line for line in self.lines if isinstance(line, MacroListLine) ]

    @property
    def actions(self):
        return [ line for line in self.lines if isinstance(line, ActionLine) ]

    @property
    def policies(self):
        return [ line for line in self.lines if isinstance(line, PolicyLine) ]
